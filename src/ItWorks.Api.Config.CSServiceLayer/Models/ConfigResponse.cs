﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItWorks.Api.Config.CSServiceLayer.Models
{
    public class ConfigResponse
    {
        public string Config { get; set; }
        public string Error { get; set; }
    }
}

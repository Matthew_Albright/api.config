﻿using ItWorks.Api.Config.CSServiceLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItWorks.Api.Config.CSServiceLayer
{
    public class Utilities
    {
        public bool IsValid(ConfigRequest request, bool isSet)
        {
            bool isVaild = true;
            if (request.AuthKey == null || request.ClientId == null || request.SecretName == null || request.VaultName == null)
            {
                isVaild = false;
            }
            if (isVaild && (request.AuthKey.Length == 0 || request.ClientId.Length == 0 || request.SecretName.Length == 0 || request.VaultName.Length == 0))
            {
                isVaild = false;
            }
            if (isVaild && isSet && (request.Secret == null || request.Secret.Length == 0))
            {
                isVaild = false;
            }
            return isVaild;
        }
    }
}

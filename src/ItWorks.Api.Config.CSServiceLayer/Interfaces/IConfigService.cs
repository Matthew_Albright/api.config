﻿using System.Collections.Generic;
using ItWorks.Api.Config.CSServiceLayer.Models;

namespace ItWorks.Api.Config.CSServiceLayer.Interfaces
{
    public interface IConfigService
    {
        ConfigResponse GetConfig(ConfigRequest model);
        ConfigResponse SetConfig(ConfigRequest model);
    }
}

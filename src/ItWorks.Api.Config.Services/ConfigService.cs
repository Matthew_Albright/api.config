﻿using ItWorks.Api.Config.CSServiceLayer;
using ItWorks.Api.Config.CSServiceLayer.Interfaces;
using CSModels = ItWorks.Api.Config.CSServiceLayer.Models;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using SRModels = ItWorks.Api.Config.SRServiceLayer.Models;
using ItWorks.Api.Config.Repositories.Utilities;
using ItWorks.AspNetCore.Logging;
using System;
using System.Threading.Tasks;

namespace ItWorks.Api.Config.Services
{
    public class ConfigService : IConfigService
    {
        private readonly ILogger _logger;
        private readonly IRepository<AzureUtilities> _configRepository;
        public ConfigService(ILogger logger, IRepository<AzureUtilities> configRepository)
        {
            _logger = logger;
            _configRepository = configRepository;
        }

        public CSModels.ConfigResponse GetConfig(CSModels.ConfigRequest model)
        {
            Utilities utilities = new Utilities();
            if (utilities.IsValid(model, false))
            {
                string config = string.Empty;
                var srModel = new SRModels.ConfigRequest()
                {
                    AuthKey = model.AuthKey,
                    SecretName = model.SecretName,
                    VaultName = model.VaultName,
                    ClientId = model.ClientId
                };

                var response = _configRepository.GetConfig(srModel);
                Task.WaitAny(response);
                return HandleResponse(response);
            }
            else
            {
                throw new AggregateException();
            }

        }

        public CSModels.ConfigResponse SetConfig(CSModels.ConfigRequest model)
        {
            Utilities utilities = new Utilities();
            if (utilities.IsValid(model, true))
            {
                var srModel = new SRModels.ConfigRequest()
                {
                    AuthKey = model.AuthKey,
                    SecretName = model.SecretName,
                    VaultName = model.VaultName,
                    ClientId = model.ClientId,
                    Secret = model.Secret
                };

                var response = _configRepository.SetConfig(srModel);
                Task.WaitAny(response);
                return HandleResponse(response);
            }
            else
            {
                throw new AggregateException();
            }

        }

        private CSModels.ConfigResponse HandleResponse(Task<SRModels.ConfigResponse> response)
        {
            CSModels.ConfigResponse handledResponse = new CSModels.ConfigResponse();
            if (response.Result == null)
            {
                handledResponse.Config = null;
            }
            else
            {
                handledResponse.Config = response.Result.Config;
            }
            return handledResponse;

        }
    }
}

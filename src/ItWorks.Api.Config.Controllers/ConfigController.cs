﻿using ItWorks.Api.Config.CSServiceLayer.Interfaces;
using ItWorks.Api.Config.CSServiceLayer.Models;
using ItWorks.Api.Config.CSServiceLayer;
using ItWorks.AspNetCore.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ItWorks.Api.Config.Controllers
{
    /// <summary>
    /// Config Controller
    /// </summary>
    [Route("v1/[controller]")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public class ConfigController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IConfigService _configService;
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigController(ILogger logger, IConfigService configService)
        {
            _logger = logger;
            _configService = configService;
        }

        /// <summary>
        /// Get Single Config
        /// </summary>
        [HttpPost("GetConfig")]
        public IActionResult GetConfig([FromBody] ConfigRequest request)
        {
            var utilities = new Utilities();
            if (!utilities.IsValid(request, false))
            {
                return BadRequest(new ConfigResponse()
                {
                    Error = "Invalid input"
                });
            }
            try
            {
                return Ok(_configService.GetConfig(request));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "GetConfig Error");
                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Trim() != "Production")
                {
                    return StatusCode(400, new ConfigResponse()
                    {
                        Error = ex.Message
                    });
                }
                else
                {
                    return BadRequest();
                }
            }

        }

        /// <summary>
        /// Sets a Single Config (Create or Edit)
        /// </summary>
        [HttpPost("SetConfig")]
        public IActionResult SetConfig([FromBody] ConfigRequest request)
        {
            var utilities = new Utilities();
            if (!utilities.IsValid(request, true))
            {
                return BadRequest();
            }
            try
            {
                return Ok(_configService.SetConfig(request));
            }
            catch (Exception ex)
            {
                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Trim() != "Production")
                {
                    return StatusCode(400, new ConfigResponse()
                    {
                        Error = ex.Message
                    });
                }
                else
                {
                    return BadRequest();
                }
            }
        }
    }
}

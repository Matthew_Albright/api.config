﻿using ItWorks.Api.Config.SRServiceLayer.Models;
using System.Threading.Tasks;

namespace ItWorks.Api.Config.SRServiceLayer.Interfaces
{
    public interface IRepository<T>
    {
        Task<ConfigResponse> GetConfig(ConfigRequest request);
        Task<ConfigResponse> SetConfig(ConfigRequest request);

    }
}

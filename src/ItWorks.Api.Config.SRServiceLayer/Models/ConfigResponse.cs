﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItWorks.Api.Config.SRServiceLayer.Models
{
    public class ConfigResponse
    {
        public string Config { get; set; }
    }
}

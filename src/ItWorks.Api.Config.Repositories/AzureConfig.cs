﻿using System;
using System.Threading.Tasks;
using ItWorks.Api.Config.Repositories.Utilities;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using ItWorks.Api.Config.SRServiceLayer.Models;
using ILogger = ItWorks.AspNetCore.Logging;

namespace ItWorks.Api.Config.Repositories
{
    public class AzureConfig : IRepository<AzureUtilities>
    {
        public Task<ConfigResponse> GetConfig(ConfigRequest model)
        {
                var provider = new AzureUtilities(model);
                var response = provider.GetConfig(model);
                return response;
        }

        public Task<ConfigResponse> SetConfig(ConfigRequest model)
        {
                var provider = new AzureUtilities(model);
                var response = provider.SetConfig(model);
                return response;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net.Http;
using System.Security.Authentication;
using ItWorks.Api.Config.SRServiceLayer;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using ItWorks.Api.Config.SRServiceLayer.Models;
using ILogger = ItWorks.AspNetCore.Logging;
using Microsoft.Rest.Azure.Authentication;

namespace ItWorks.Api.Config.Repositories.Utilities
{
    public class AzureUtilities : IRepository<AzureUtilities> 
    {
        private const string _authority = "https://login.windows.net/373afff1-2527-439e-a138-0cd72c9325e4";
        private const string _resource = "https://vault.azure.net";
        string _clientSecret;
        string _clientId;
        
        public AzureUtilities(ConfigRequest model)
        {
            _clientSecret = model.AuthKey;
            _clientId = model.ClientId;
        }
        public async Task<ConfigResponse> GetConfig(ConfigRequest model)
        {
            var response = new ConfigResponse();
            var result = new SecretBundle();
            using (var keyVaultClient = new KeyVaultClient(AuthenticateVault))
            {
                result = await keyVaultClient.GetSecretAsync(string.Format("https://{0}.vault.azure.net/secrets/{1}", model.VaultName, model.SecretName));
                keyVaultClient.Dispose();
            };
            response.Config = result.Value;
            return response;
        }

        public async Task<ConfigResponse> SetConfig(ConfigRequest model)
        {
            var response = new ConfigResponse();
            var result = new SecretBundle();
            using (var keyVaultClient = new KeyVaultClient(AuthenticateVault))
            {
                result = await keyVaultClient.SetSecretAsync(string.Format("https://{0}.vault.azure.net/", model.VaultName), model.SecretName, model.Secret, null, null, CreateSecretAttributes());
                keyVaultClient.Dispose();
            };
            response.Config = result.Value;
            return response;
        }

        private async Task<string> AuthenticateVault(string authority = _authority, string resource = _resource, string scope = "")
        {
            var clientCredentials = new ClientCredential(_clientId, _clientSecret);
            var authenticationContext = new AuthenticationContext(authority);
            var result = await authenticationContext.AcquireTokenAsync(resource, clientCredentials);
            return result.AccessToken;
        }

        public SecretAttributes CreateSecretAttributes()
        {
            var attributes = new SecretAttributes()
            {
                Enabled = true,
                Expires = DateTime.Now.AddYears(30),
                NotBefore = DateTime.Now.AddDays(-1),
            };
            return attributes;
        }
    }
}

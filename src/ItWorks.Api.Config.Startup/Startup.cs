﻿using System;
using Exceptionless;
using ItWorks.Api.Config.CSServiceLayer.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog.Sinks;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using System.Net.Http;
using System.Security.Authentication;
using ItWorks.Api.Config.Repositories.Utilities;
using ItWorks.Api.Config.Repositories;
using ItWorks.Api.Config.Services;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using ItWorks.Api.Config.Startup.Middleware;
using ItWorks.AspNetCore.Logging;
using ILogger = ItWorks.AspNetCore.Logging.ILogger;

namespace ItWorks.Api.Config.Startup
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }
        public IHostingEnvironment Environment { get; set; }
        public ILogger Log { get; set; }

        private const string ServiceName = "ItWorks.Api.Config";
        private const string DefaultCorsPolicy = "DefaultCorsPolicy";

        public Startup(IHostingEnvironment env)
        {
            Environment = env;

            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.secrets.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            Environment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddItWorksLogging();

            ConfigureDepenencies(services);

            AddCors(services, DefaultCorsPolicy);

            ConfigureServicePointManager();

            services.AddMvc();

            services.AddSingleton<IConfiguration>(Configuration);

            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMemoryCache();

            services.AddMvcCore();

            ConfigureSwagger(services);

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger log)
        {
            app.UseItWorksLogging(env, Configuration);
            Log = log;

            app.UseCors(DefaultCorsPolicy);
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });

            if (!env.IsProduction())
            {
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Config API V1");

                });
            }

            app.UseUnknownRoute();
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddMvcCore()
                .AddApiExplorer();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = $"{ServiceName} Service",
                        Version = "v1",
                        Description = $"Service for handling {ServiceName} specific back-end operations.",
                        TermsOfService = "API Terms"
                    }
                );
                c.DescribeAllEnumsAsStrings();

                var path = Path.Combine(Environment.ContentRootPath, $"{ServiceName}.Controllers.xml");
                c.IncludeXmlComments(path);

            });

        }

        private void ConfigureDepenencies(IServiceCollection services)
        {
            services.AddScoped<IConfigService, ConfigService>();
            services.AddScoped<IRepository<AzureUtilities>, AzureUtilities>();
            services.AddScoped<IRepository<AzureUtilities>, AzureConfig>();
        }

        private void ConfigureServicePointManager()
        {
            WinHttpHandler httpHandler = new WinHttpHandler()
            {
                SslProtocols = SslProtocols.Tls12,
                MaxConnectionsPerServer = Configuration.GetValue<int>("AppSettings:DefaultConnectionLimit")
            };
        }

        private static void AddCors(IServiceCollection services, string corsPolicy)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(corsPolicy,
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
            });
        }
    }
}

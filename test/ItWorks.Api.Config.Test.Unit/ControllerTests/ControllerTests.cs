﻿using ItWorks.Api.Config;
using ItWorks.Api.Config.Repositories;
using ItWorks.Api.Config.Services;
using ItWorks.Api.Config.Controllers;
using ServiceModels = ItWorks.Api.Config.SRServiceLayer.Models;
using ItWorks.Api.Config.CSServiceLayer;
using ItWorks.Api.Config.CSServiceLayer.Interfaces;
using ControllerModels = ItWorks.Api.Config.CSServiceLayer.Models;
using ItWorks.AspNetCore.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace ItWorks.Api.Config.Test.Unit.ControllerTests
{
    [TestClass]
    public class ControllerTests
    {
        private Mock<IConfigService> _service;
        private Mock<ILogger> _loggerMock = new Mock<ILogger>();
        private Mock<AzureConfig> _configMock = new Mock<AzureConfig>();
        private CSServiceLayer.Models.ConfigResponse _controllerResponse;


        [TestMethod]
        public void Valid_GetRequest_Expecting_Secret()
        {
            _controllerResponse = new ControllerModels.ConfigResponse()
            {
                Config = "good"
            };
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object ).GetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsNotNull(_controllerResponse.Config);
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
        }

        [TestMethod]
        public void Valid_SetRequest_Expecting_Secret()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
        }

        [TestMethod]
        public void InvalidKey_GetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse()
            {
                Error = "Invalid input"
            };
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));
            Assert.IsNotNull(_controllerResponse.Error);
        }

        [TestMethod]
        public void InvalidClient_GetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "",
                SecretName = "this is a test",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));

        }

        [TestMethod]
        public void InvalidSecretName_GetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void InvalidVaultName_GetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = ""
            };
            _service.Setup(a => a.GetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void ValidUtility_GetRequest_Expecting_True()
        {
            Utilities utilities = new Utilities();
            bool isValid = utilities.IsValid(new ControllerModels.ConfigRequest()
            {
                AuthKey = "valid",
                ClientId = "valid",
                SecretName = "valid",
                VaultName = "valid"
            }, false);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void ValidUtility_SetRequest_Expecting_True()
        {
            Utilities utilities = new Utilities();
            bool isValid = utilities.IsValid(new ControllerModels.ConfigRequest()
            {
                AuthKey = "valid",
                ClientId = "valid",
                SecretName = "valid",
                VaultName = "valid",
                Secret = "valid"
            }, true);
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void InvalidKey_SetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void InvalidClient_SetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "",
                SecretName = "this is a test",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void InvalidSecretName_SetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void InvalidVaultName_SetRequest_Expecing_Failure()
        {
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Returns(_controllerResponse);

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void Valid_Production_SetRequest_Expecing_Exception()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production");
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Throws(new InvalidCastException());

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void Valid_Production_GetRequest_Expecing_Exception()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production");
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Throws(new InvalidCastException());

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsInstanceOfType(response, typeof(BadRequestResult));
        }

        [TestMethod]
        public void Valid_Test_SetRequest_Expecing_Exception()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Test");
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test",
                Secret = "this is a test"
            };
            _service.Setup(a => a.SetConfig(configRequest)).Throws(new InvalidCastException());

            var response = new ConfigController(_loggerMock.Object, _service.Object).SetConfig(configRequest);
            Assert.IsInstanceOfType(response, typeof(ObjectResult));
        }

        [TestMethod]
        public void Valid_Test_GetRequest_Expecing_Exception()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Test");
            _controllerResponse = new ControllerModels.ConfigResponse();
            _service = new Mock<IConfigService>();
            var configRequest = new ControllerModels.ConfigRequest
            {
                AuthKey = "this is a test",
                ClientId = "this is a test",
                SecretName = "this is a test",
                VaultName = "this is a test"
            };
            _service.Setup(a => a.GetConfig(configRequest)).Throws(new InvalidCastException());

            var response = new ConfigController(_loggerMock.Object, _service.Object).GetConfig(configRequest);
            Assert.IsInstanceOfType(response, typeof(ObjectResult));
        }








    }
}

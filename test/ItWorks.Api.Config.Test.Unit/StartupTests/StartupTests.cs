﻿using Exceptionless;
using ItWorks.Api.Config.Startup.Middleware;
using ItWorks.AspNetCore.Logging;
using ILogger = ItWorks.AspNetCore.Logging.ILogger;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace ItWorks.Api.Config.Test.Unit.StartupTests
{
    [TestClass]
    public class StartupTests
    {
        [TestMethod]
        public void Invalid_Environment_Startup_ExpectingFailure()
        {
            try
            {
                var server = new TestServer(new WebHostBuilder()
                    .UseStartup<Startup.Startup>()
                    .UseEnvironment("Bad"));
                server.CreateClient();
                Assert.Fail();
            }
            catch (System.IO.FileNotFoundException)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void Valid_Environment_Startup_ExpectingSuccess()
        {
            try
            {
                var server = new TestServer(new WebHostBuilder()
                    .UseStartup<Startup.Startup>()
                    .UseEnvironment("Test"));
                server.CreateClient();
                Assert.IsTrue(true);
            }
            catch (System.IO.FileNotFoundException)
            {
                Assert.Fail();
            }
        }
    }
}

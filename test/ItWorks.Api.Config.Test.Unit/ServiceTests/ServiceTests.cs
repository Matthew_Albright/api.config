﻿using ItWorks.Api.Config;
using ItWorks.Api.Config.Repositories;
using ItWorks.Api.Config.Repositories.Utilities;
using ItWorks.Api.Config.Services;
using ItWorks.Api.Config.Controllers;
using SRModels = ItWorks.Api.Config.SRServiceLayer.Models;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using ItWorks.Api.Config.CSServiceLayer.Interfaces;
using CSModels = ItWorks.Api.Config.CSServiceLayer.Models;
using ItWorks.AspNetCore.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace ItWorks.Api.Config.Test.Unit.ServiceTests
{
    [TestClass]
    public class ServiceTests
    {
        private Mock<ILogger> _loggerMock = new Mock<ILogger>();
        private SRModels.ConfigResponse _mockRepoResponse = new SRModels.ConfigResponse()
        {
            Config = "valid",
        };

        [TestMethod]
        public void Valid_GetRequest_Service_ExpectSuccess()
        {
            Mock<IRepository<AzureUtilities>> mockUtilities = new Mock<IRepository<AzureUtilities>>();
            Mock<IConfigService> mockService = new Mock<IConfigService>();

            CSModels.ConfigRequest controllerRequest = new CSModels.ConfigRequest()
            {
                AuthKey = "test",
                SecretName = "test",
                ClientId = "test",
                VaultName = "test"
            };

            SRModels.ConfigRequest serviceRequest = new SRModels.ConfigRequest()
            {
                AuthKey = controllerRequest.AuthKey,
                SecretName = controllerRequest.SecretName,
                ClientId = controllerRequest.ClientId,
                VaultName = controllerRequest.VaultName
            };

            mockUtilities.Setup(a => a.GetConfig(serviceRequest)).ReturnsAsync(_mockRepoResponse);
            var response = new ConfigService(_loggerMock.Object, mockUtilities.Object).GetConfig(controllerRequest);
            Assert.AreEqual(response.GetType().ToString(), new CSModels.ConfigResponse().GetType().ToString());
        }

        [TestMethod]
        public void Valid_SetRequest_Service_ExpectSuccess()
        {
            Mock<IRepository<AzureUtilities>> mockUtilities = new Mock<IRepository<AzureUtilities>>();
            Mock<IConfigService> mockService = new Mock<IConfigService>();

            CSModels.ConfigRequest controllerRequest = new CSModels.ConfigRequest()
            {
                AuthKey = "test",
                SecretName = "test",
                ClientId = "test",
                VaultName = "test",
                Secret = "test"
            };

            SRModels.ConfigRequest serviceRequest = new SRModels.ConfigRequest()
            {
                AuthKey = controllerRequest.AuthKey,
                SecretName = controllerRequest.SecretName,
                ClientId = controllerRequest.ClientId,
                VaultName = controllerRequest.VaultName
            };

            mockUtilities.Setup(a => a.SetConfig(serviceRequest)).ReturnsAsync(_mockRepoResponse);
            var response = new ConfigService(_loggerMock.Object, mockUtilities.Object).SetConfig(controllerRequest);
            Assert.IsInstanceOfType(response, typeof(CSModels.ConfigResponse));
            Assert.AreEqual(response.GetType().ToString(), new CSModels.ConfigResponse().GetType().ToString());
        }

        [TestMethod]
        public void Invalid_Null_SetRequest_Service_Expecting_Error()
        {
            Mock<IRepository<AzureUtilities>> mockUtilities = new Mock<IRepository<AzureUtilities>>();
            Mock<IConfigService> mockService = new Mock<IConfigService>();

            CSModels.ConfigRequest controllerRequest = new CSModels.ConfigRequest()
            {
                SecretName = "test",
                ClientId = "test",
                VaultName = "test"
            };

            SRModels.ConfigRequest serviceRequest = new SRModels.ConfigRequest()
            {
                SecretName = controllerRequest.SecretName,
                ClientId = controllerRequest.ClientId,
                VaultName = controllerRequest.VaultName
            };

            mockUtilities.Setup(a => a.SetConfig(serviceRequest)).ReturnsAsync(_mockRepoResponse);
            try
            {
                var response = new ConfigService(_loggerMock.Object, mockUtilities.Object).SetConfig(controllerRequest);
                Assert.Fail();
            }
            catch (AggregateException)
            {
                Assert.IsTrue(true);
            }
            
        }


    }
}

using ItWorks.Api.Config;
using ItWorks.Api.Config.Repositories;
using ItWorks.Api.Config.Repositories.Utilities;
using ItWorks.Api.Config.Services;
using ItWorks.Api.Config.SRServiceLayer.Models;
using ItWorks.Api.Config.SRServiceLayer.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace ItWorks.Api.Config.Test.Unit
{
    [TestClass]
    public class RepoTests
    {
        [TestMethod]
        public void Valid_Request_Expecting_Secret()
        {
            var response = new AzureConfig().GetConfig(new ConfigRequest()
            {
                AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                SecretName = "AlbrightSecret",
                VaultName = "albrightkeyvault"
            }).Result;
            Assert.IsNotNull(response.Config);
        }

        [TestMethod]
        public void Invalid_Request_Bad_ValutName_Expecting_Error()
        {
            Exception ex = new Exception();
            try
            {
                var config = new AzureConfig().GetConfig(new ConfigRequest()
                {
                    AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                    ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                    SecretName = "AlbrightSecret",
                    VaultName = "BadVaultName"
                }).Result;
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                Assert.IsTrue(ex.Message.Contains("One or more errors occurred. (An error occurred while sending the request.)"));
            }
        }

        [TestMethod]
        public void Invalid_Request_Bad_SecretName_Expecting_Error()
        {
            Exception ex = new Exception();
            try
            {
                var response = new AzureConfig().GetConfig(new ConfigRequest()
                {
                    AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                    ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                    SecretName = "BadSecretName",
                    VaultName = "albrightkeyvault"
                }).Result;
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                Assert.IsTrue(ex.Message.Contains("One or more errors occurred"));
            }


        }

        [TestMethod]
        public void Invalid_Request_Bad_AuthKey_Expecting_Error()
        {
            Exception ex = new Exception();
            try
            {
                var response = new AzureConfig().GetConfig(new ConfigRequest()
                {
                    AuthKey = "Testing",
                    ClientId = "Testing",
                    SecretName = "AlbrightSecret",
                    VaultName = "albrightkeyvault"
                }).Result;
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                Assert.IsTrue(ex.Message.Contains("One or more errors occurred"));
            }
        }

        [TestMethod]
        public void Invalid_Request_Bad_ClientId_Expecting_Error()
        {
            Exception ex = new Exception();
            try
            {
                var response = new AzureConfig().GetConfig(new ConfigRequest()
                {
                    AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                    ClientId = "BadClientId",
                    SecretName = "AlbrightSecret",
                    VaultName = "albrightkeyvault"
                }).Result;
            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                Assert.IsTrue(ex.Message.Contains("One or more errors occurred"));
            }
        }

        [TestMethod]
        public void Valid_Request_Utilities_Expecting_Success()
        {
            ConfigRequest request = new ConfigRequest()
            {
                AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                SecretName = "AlbrightSecret",
                VaultName = "albrightkeyvault"
            };
            AzureUtilities utilities = new AzureUtilities(request);

            var response = utilities.GetConfig(request).Result;

            Assert.IsNotNull(response.Config);

        }

        [TestMethod]
        public void Valid_SetRequest_Utilities_Expecting_Success()
        {
            ConfigRequest request = new ConfigRequest()
            {
                AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                SecretName = "AlbrightTopSecret",
                VaultName = "albrightkeyvault",
                Secret = "thisisatest"
            };
            AzureUtilities utilities = new AzureUtilities(request);

            var response = utilities.SetConfig(request).Result;

            Assert.IsNotNull(response.Config);

        }

        [TestMethod]
        public void Invalid_Request_Utilities_Epecting_Error()
        {
            Exception ex = new Exception();
            try {
                ConfigRequest request = new ConfigRequest()
                {
                    AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                    ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                    SecretName = "AlbrightSecret",
                    VaultName = "badvaultname"
                };
                AzureUtilities utilities = new AzureUtilities(request);

                var response = utilities.GetConfig(request).Result;

            }
            catch (Exception e)
            {
                ex = e;
            }
            finally
            {
                Assert.IsTrue(ex.Message.Contains("One or more errors occurred"));
            }
        }

        [TestMethod]
        public void Valid_CreateSecretAttributes_Expecting_Success()
        {
            ConfigRequest request = new ConfigRequest()
            {
                AuthKey = "test",
                ClientId = "test",
                SecretName = "test",
                VaultName = "test"
            };
            AzureUtilities utilities = new AzureUtilities(request);
            var secretAttributes = utilities.CreateSecretAttributes();
            Assert.IsTrue(!string.IsNullOrEmpty(secretAttributes.ToString()));
        }

        [TestMethod]
        public void Valid_AzureConfig_SetRequest_Expecting_Success()
        {
            ConfigRequest request = new ConfigRequest()
            {
                AuthKey = " FGEky3gk6ginO63mjUerwiXvki5N9f/EbnyWUGeTjM0=",
                ClientId = "1fcdac87-8a16-4b7f-8950-deb7fcbb71fc",
                SecretName = "AlbrightTopSecret",
                VaultName = "albrightkeyvault",
                Secret = "thisisatest"
            };

            AzureConfig utilities = new AzureConfig();
            var response = utilities.SetConfig(request).Result;
            Assert.IsNotNull(response.Config);
        }

    }
}
